<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main.index');
});

Route::get('/', [\App\Http\Controllers\Admin\CategoryController::class, 'main_category_index'])->name('main.index');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/admins', [\App\Http\Controllers\HomeController::class, 'adminpanel'])->name('adminpanel');
Route::get('/users', [\App\Http\Controllers\HomeController::class, 'userpanel'])->name('userpanel');

Route::get('/admins/create', [\App\Http\Controllers\Admin\CategoryController::class, 'create'])->name('category.create');
Route::post('/admins/add-category', [\App\Http\Controllers\Admin\CategoryController::class, 'store'])->name('store.category');
Route::get('/admins/view-category', [\App\Http\Controllers\Admin\CategoryController::class, 'index'])->name('category.index');
Route::get('/admins/delete-category/{id}', [\App\Http\Controllers\Admin\CategoryController::class, 'delete'])->name('category.delete');
Route::get('/admins/edit-category/{id}', [\App\Http\Controllers\Admin\CategoryController::class, 'edit'])->name('category.edit');
Route::put('/admins/update-category/{id}', [\App\Http\Controllers\Admin\CategoryController::class, 'update'])->name('category.update');
Route::get('/admins/add-subcategory', [\App\Http\Controllers\Admin\SubcategoryController::class, 'create'])->name('subcategory.create');
Route::post('/admins/add-subctg', [\App\Http\Controllers\Admin\SubcategoryController::class, 'yaratish'])->name('subcategory.yaratish');
Route::get('/admins/view-subcategory', [\App\Http\Controllers\Admin\SubcategoryController::class, 'index'])->name('subcategory.index');
