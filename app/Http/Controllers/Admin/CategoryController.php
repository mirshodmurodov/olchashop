<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function create()
    {
        return view('admin.category.create');
    }

    public function index()
    {
        $categories = Category::all();

        return view('admin.category.view', compact('categories'));
    }


    public function store(Request $request)
    {
//        dd($request->input('title'));
        $title = $request->input('title');
        DB::table('categories')->insert([
            'title'=>$title
        ]);
        return redirect()->route('category.index');
//        DB::select('insert into categories(title) values(?)',[$title]);
    }

    public function delete($id)
    {
        DB::select('delete from categories where id = ?',[$id]);
        return redirect()->route('category.index');
    }

    public function edit($id)
    {
        $category = DB::select('select * from categories where id = ?', [$id]);
//        dd($category[0]->title);
        return view('admin.category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $title = $request->input('title');
        DB::select('update categories set title = ? where id = ?',[$title,$id]);
        return redirect()->route('category.index');
    }

    public function main_category_index()
    {
        $categories = Category::all();
        return view('main.index', compact('categories'));
    }
}
