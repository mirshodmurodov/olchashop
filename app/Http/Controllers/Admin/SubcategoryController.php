<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubcategoryController extends Controller
{
    public function index()
    {
        $subcategories = Subcategory::all();
        return view('admin.subcategory.view', compact('subcategories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.subcategory.create', compact('categories'));
    }

    public function yaratish(Request $request)
    {
        $category_id = $request->input('category_id');
        $sana = count($request->subcategory_title);
        for ($i=0; $i<$sana; $i++)
        {
            DB::table('subcategories')->insert([
                'category_id'=>$category_id,
                'subcategory_title'=>$request->input('subcategory_title')[$i]
            ]);
        }
        return redirect()->route('subcategory.index');
    }
}
