<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'category_id',
        'subcategory_title'
    ];

    public function category_name()
    {
        return $this->belongsTo(Category::class, 'category_id','id');
    }
}
