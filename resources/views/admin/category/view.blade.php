@extends('admin.layouts.master')
@section('title', 'Categories')
@section('content')
    <div class="col-lg-12 grid-margin stretch-card pt-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">View categories</h4>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title category</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                    <tr>
                        <td>{{$loop->index+1}}</td>
                        <td>{{$category->title}}</td>
                        <td><a href="" class="btn btn-primary">View</a></td>
                        <td><a href="{{route('category.edit', $category->id)}}" class="btn btn-primary">Edit</a></td>
                        <td><a href="{{route('category.delete', $category->id)}}" class="btn btn-danger">Delete</a></td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="5">Not datas to DB</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
