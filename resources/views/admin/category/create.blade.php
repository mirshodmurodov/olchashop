@extends('admin.layouts.master')
@section('title','Create category')
@section('content')
    <div class="col-12 grid-margin pt-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Add category</h4>

                <form action="{{route('store.category')}}" method="post" class="forms-sample">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category title</label>
                        <input name="title" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter category name">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Create category</button>
                </form>
            </div>
        </div>
    </div>
@endsection
