@extends('admin.layouts.master')
@section('title','Edit category')
@section('content')
    <div class="col-12 grid-margin pt-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Edit category</h4>

                <form action="{{route('category.update', $category[0]->id)}}" method="post" class="forms-sample">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category title</label>
                        <input value="{{$category[0]->title}}" name="title" type="text" class="form-control" id="exampleInputEmail1">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Update category</button>
                </form>
            </div>
        </div>
    </div>
@endsection
