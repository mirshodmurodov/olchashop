<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title')</title>
    <meta name="theme-name" content="mono" />
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700|Roboto" rel="stylesheet">
    <link href="{{asset('admin/plugins/material/css/materialdesignicons.min.css')}}" rel="stylesheet" />
    <link href="{{asset('admin/plugins/simplebar/simplebar.css')}}" rel="stylesheet" />
    <link href="{{asset('admin/plugins/nprogress/nprogress.css')}}" rel="stylesheet" />
    <link href="{{asset('admin/plugins/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css')}}" rel="stylesheet" />
    <link href="{{asset('admin/plugins/jvectormap/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet" />
    <link href="{{asset('admin/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('admin/https://cdn.quilljs.com/1.3.6/quill.snow.css')}}" rel="stylesheet">
    <link href="{{asset('admin/plugins/toaster/toastr.min.css')}}" rel="stylesheet" />
    <link id="main-css-href" rel="stylesheet" href="{{asset('admin/css/style.css')}}" />
    <link href="{{asset('admin/images/favicon.png')}}" rel="shortcut icon" />
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="{{asset('admin/plugins/nprogress/nprogress.js')}}"></script>
</head>
<body class="navbar-fixed sidebar-fixed" id="body">
<div class="wrapper">

   @include('admin.partials.sidebar')
    <div class="page-wrapper">
        @include('admin.partials.navbar')
        <div class="content-wrapper">
           @yield('content')
        </div>
    </div>
</div>
<script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('admin/plugins/simplebar/simplebar.min.js')}}"></script>
<script src="{{asset('admin/https://unpkg.com/hotkeys-js/dist/hotkeys.min.js')}}"></script>
<script src="{{asset('admin/plugins/apexcharts/apexcharts.js')}}"></script>
<script src="{{asset('admin/plugins/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill.js')}}"></script>
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-us-aea.js')}}"></script>
<script src="{{asset('admin/plugins/daterangepicker/moment.min.js')}}"></script>
<script src="{{asset('admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script>
    jQuery(document).ready(function() {
        jQuery('input[name="dateRange"]').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });
        jQuery('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
            jQuery(this).val(picker.startDate.format('MM/DD/YYYY'));
        });
        jQuery('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
            jQuery(this).val('');
        });
    });
</script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="{{asset('admin/plugins/toaster/toastr.min.js')}}"></script>
<script src="{{asset('admin/js/mono.js')}}"></script>
<script src="{{asset('admin/js/chart.js')}}"></script>
<script src="{{asset('admin/js/map.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>
</body>
</html>

