@extends('admin.layouts.master')
@section('title', 'Subcategories')
@section('content')
    <div class="col-lg-12 grid-margin stretch-card pt-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">View subcategories</h4>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title category</th>
                        <th>Title subcategory</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($subcategories as $subcategory)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$subcategory->category_name->title}}</td>
                            <td>{{$subcategory->subcategory_title}}</td>
                            <td><a href="" class="btn btn-primary">Edit</a></td>
                            <td><a href="" class="btn btn-danger">Delete</a></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Not datas to DB</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
