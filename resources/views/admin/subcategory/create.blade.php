@extends('admin.layouts.master')
@section('title','Create subcategory')
@section('content')
    <div class="col-12 grid-margin pt-3">
        <div class="card">
            <div class="card-body">
                <div class="rowik d-flex align-items-center justify-content-between">
                <h4 class="card-title">Add subcategory</h4>
                <button id="add_input" class="btn btn-success">+</button>
                </div>
                <form action="{{route('subcategory.yaratish')}}" method="post" class="forms-sample">
                    @csrf
                    <div class="for_add_tag" id="asosiy">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select category title</label>
                            <select class="form-control" name="category_id" id="">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Create subcategory</button>
                </form>
            </div>
        </div>
    </div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function (){
        $('#add_input').click(function (e)
        {
            $('#asosiy').append(`
                <label for="">Subcategory title</label>
                <div class="for_rem d-flex align-items-center">
                    <div class="form-group col-md-11">

                       <input type="text" class="form-control" name="subcategory_title[]">
                    </div>
                    <div class="form-group col-md-1">
                        <button id="rm_btn" class="btn btn-danger">x</button>
                    </div>
                </div>
            `);
        });
    });

    $(document).on('click', '#rm_btn', function (e){
        let row_item = $(this).parent().parent();
        $(row_item).remove();
    });
</script>
