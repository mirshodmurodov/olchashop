<aside class="left-sidebar sidebar-dark" id="left-sidebar">
    <div id="sidebar" class="sidebar sidebar-with-footer">
        <!-- Aplication Brand -->
        <div class="app-brand">
            <a href="/index.html">
                <img src="{{asset('admin/images/logo.png')}}" alt="Mono">
                <span class="brand-name">OnLiNe ShoP</span>
            </a>
        </div>
        <!-- begin sidebar scrollbar -->
        <div class="sidebar-left" data-simplebar style="height: 100%;">
            <!-- sidebar menu -->
            <ul class="nav sidebar-inner" id="sidebar-menu">
                <li class="has-sub">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#category"
                       aria-expanded="false" aria-controls="category">
                        <i class="mdi mdi-tab"></i>
                        <span class="nav-text">Categories</span> <b class="caret"></b>
                    </a>
                    <ul  class="collapse {{(request()->is('admins/create') || request()->is('admins/view-category')) ? 'd-block' : ''}}"  id="category"
                         data-parent="#sidebar-menu">
                        <div class="sub-menu">
                            <li >
                                <a style="color: {{request()->is('admins/create') ? '#9E6DE2':''}}" class="sidenav-item-link" href="{{route('category.create')}}">
                                    <span class="nav-text">Add category</span>
                                </a>
                            </li>
                            <li>
                                <a style="color: {{request()->is('admins/view-category') ? '#9E6DE2':''}}" class="sidenav-item-link" href="{{route('category.index')}}">
                                    <span class="nav-text">View categories</span>
                                </a>
                            </li>
                        </div>
                    </ul>
                </li>



                <li class="has-sub ">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#subcategory"
                       aria-expanded="false" aria-controls="subcategory">
                        <i class="mdi mdi-tab"></i>
                        <span class="nav-text">Subcategories</span> <b class="caret"></b>
                    </a>
                    <ul  class="collapse {{(request()->is('admins/add-subcategory') || request()->is('admins/view-subcategory')) ? 'd-block' : ''}}"  id="subcategory"
                         data-parent="#sidebar-menu">
                        <div class="sub-menu">
                            <li >
                                <a style="color: {{request()->is('admins/add-subcategory') ? '#9E6DE2':''}}" class="sidenav-item-link" href="{{route('subcategory.create')}}">
                                    <span class="nav-text">Add subcategory</span>
                                </a>
                            </li>
                            <li>
                                <a style="color: {{request()->is('admins/view-subcategory') ? '#9E6DE2':''}}" class="sidenav-item-link" href="{{route('subcategory.index')}}">
                                    <span class="nav-text">View subcategories</span>
                                </a>
                            </li>
                        </div>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="sidebar-footer">
            <div class="sidebar-footer-content">
                <ul class="d-flex">
                    <li>
                        <a href="user-account-settings.html" data-toggle="tooltip" title="Profile settings"><i class="mdi mdi-settings"></i></a></li>
                    <li>
                        <a href="#" data-toggle="tooltip" title="No chat messages"><i class="mdi mdi-chat-processing"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</aside>
